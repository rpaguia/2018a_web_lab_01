Herbivorous
* Horse
* Cow
* Sheep

Carnivorous
* Dog
* Rat
* Hawk
* Lion
* Cat
* Fox

Omnivorous
* Frog
* Polar Bear
* Capybara
* Raccoon
* Chicken
* Fennec Fox