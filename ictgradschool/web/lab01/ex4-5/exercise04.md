Exercise 04
===========
RONNIE PAGUIA
Welcome to Markdown!

Markdown is a text-to-HTML conversion tool for web writers. 
Markdown allows you to write using an easy-to-read, easy-to-write 
plain text format, then convert it to structurally valid HTML.

Favorite Things
===============

These are a few of my favorite things

1. Raindrops on roses, whiskers on kittens, bright copper kettles and warm woolen mittens.
2. Brown paper packages tied up with strings, cream-colored ponies and crisp apple strudels
3. Doorbells and sleigh bells, schnitzel with noodles, wild geese that fly with the moon on their wings.
4. Girls in white dresses with blue satin sashes, snowflakes that stay on my nose and eyelashes, silver-white winters that melt into springs.

